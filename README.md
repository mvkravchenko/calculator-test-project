# calculator-test-project

Проект представляет собой калькулятор для деления 2х чисел.

1. mvn clean install - сборка с запуском Unit-тестов
2. mvn test - запуск только тестов
3. java -cp target/calculator-1.0.jar com.example.project.Starter - запуск приложения из командной строки (из корневой директории проекта)
